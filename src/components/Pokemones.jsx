import React from "react";
//useDispatch: consume la accion
//useSelector para leer el state
import {useDispatch, useSelector} from "react-redux";
import {obtenerPokemonesAccion, siguientePokemonsAction} from "../redux/pokeDucks";



const Pokemones = () => {

    const dispatch = useDispatch();
    const pokemones = useSelector(store => store.pokemones.array)

    return (
        <div>
            lista de pokemones
            <button onClick={() => {
                dispatch(obtenerPokemonesAccion())
            }}>
                Get Pokemones
            </button>

            <button onClick={() => {
                dispatch(siguientePokemonsAction(20))
            }}>
                Siguientes Pokemones
            </button>

            <hr/>

            <ul>
                {
                    pokemones.map((item) => {
                        return(
                            <li key={item.name}>{item.name}</li>
                        )
                    })
                }
            </ul>

        </div>
    )

}

export default Pokemones;