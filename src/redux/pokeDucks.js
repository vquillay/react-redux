import axios from "axios";

// constants: lo podemos consumir en algun componente
const dataInitial = {
    /*Contiene el estado inicial*/
    array : [ ],
    offset: 0,
}

/*Los types son constantes en mayusculas que describe a las actions*/
const OBTENER_POKEMONES_EXITO = 'OBTENER_POKEMONES_EXITO';
const SIGUIENTE_POKEMONES_EXITO = 'SIGUIENTE_POKEMONES_EXITO';

/* reducer: va a aceptar la lista de pokemones y los va a enviar
* a una constante o a un estado
* */
export default function pokeReducer(state = dataInitial, action){
    switch (action.type){
        case OBTENER_POKEMONES_EXITO:
            return {...state, array: action.payload}

        case SIGUIENTE_POKEMONES_EXITO:
            return {
                ...state,
                array: action.payload.array,
                offset: action.payload.offset
            }

        default:
            return state;
    }
}

// actions: va a consumir a la api, la lista de pokemones
export const obtenerPokemonesAccion = () => async (dispatch, getState) => {

    // const offset = getState().pokemones.offset;
    const {offset} = getState().pokemones;

    try {
        const res = await axios.get(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=20`);
        dispatch({
            type: OBTENER_POKEMONES_EXITO,
            payload: res.data.results
        })
    } catch (e) {
        console.log(e)
    }
}

export const siguientePokemonsAction = (numero) => async (dispatch, getState) => {

    const {offset} = getState().pokemones;
    const siguiente = offset + numero;

    try {
        const res = await axios.get(`https://pokeapi.co/api/v2/pokemon?offset=${siguiente}&limit=20`);
        dispatch({
            type: SIGUIENTE_POKEMONES_EXITO,
            payload: {
                array: res.data.results,
                offset: siguiente
            }
        })
    } catch (e) {
        console.log(e)
    }
}