/*
 * Une a todos los ducks
 */
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";

import pokeReducer from "./pokeDucks";

const rootReducer = combineReducers({
    pokemones: pokeReducer,
    // otros reducers
    // usuarios: usuariosReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore(){
    return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
}